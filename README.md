# README #

This repository includes the benchmarks used in the following paper:

Ivan Serina, Mauro Vallati, Configurable Heuristic Adaptation for Improving Best First Search in AI Planning, In The annual IEEE International Conference on Tools with Artificial Intelligence (ICTAI), 2020.

# STRUCTURE
Each benchmark domain is stored in a dedicated archive. Inside the archive, you'll find the domain model, a list of randomly generated problem models, and the list of instances used for training and testing.
The lists are provided in 2 dedicated txt files. 

Below you can find the list of archives.
# Blocksworld
https://bitbucket.org/maurovallati/ictai-2020/downloads/BlocksWorld.zip

# Depots
https://bitbucket.org/maurovallati/ictai-2020/downloads/Depots.zip

# Hiking
https://bitbucket.org/maurovallati/ictai-2020/downloads/Hiking.zip

# Logistics
https://bitbucket.org/maurovallati/ictai-2020/downloads/Logistics.zip

# Maintenance
https://bitbucket.org/maurovallati/ictai-2020/downloads/Maintenance.zip

# MatchingBW
https://bitbucket.org/maurovallati/ictai-2020/downloads/MatchingBW.zip

# Tetris
https://bitbucket.org/maurovallati/ictai-2020/downloads/Tetris.zip

# ZenoTravel
https://bitbucket.org/maurovallati/ictai-2020/downloads/ZenoTravel.zip
